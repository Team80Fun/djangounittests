
# Create your views here.
from django.shortcuts import render
from django.views import View
from team80FunUnitTests2.models import YourClass
# Create your views here.
class Home(View):
  def get(self,request):
    return render(request, 'team80FunUnitTests2/index.html')
  def post(self,request):
    yourInstance = YourClass()
    commandInput = request.POST["command"]
    if commandInput:
      response = yourInstance.command(commandInput)
    else:
      response = ""
    return render(request, 'team80FunUnitTests2/index.html',{"message":response})